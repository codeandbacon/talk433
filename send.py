
import machine
import binascii
from utime import sleep_ms

class Sender(object):

    def __init__(self, pin_number, pulse_len=10):
        self.tx_pin = machine.Pin(15, machine.Pin.OUT)
        self.pulse_len = pulse_len

    def _send_one(self):
        self.tx_pin.value(1)
        sleep_ms(self.pulse_len * 3)
        self.tx_pin.value(0)
        sleep_ms(self.pulse_len)

    def _send_zero(self):
        self.tx_pin.value(1)
        sleep_ms(self.pulse_len)
        self.tx_pin.value(0)
        sleep_ms(self.pulse_len * 3)

    def send_string(self, string):
        bins = [bin(s) for s in bytearray(string, 'utf-8')]
        bins = [s[2:] for s in bins]
        for bin_val in bins:
            sign = [s for s in bin_val]
            print(sign)
            self.send(sign)

    def send(self, signal):
        for v in signal:
            if v:
                self._send_one()
            else:
                self._send_zero()

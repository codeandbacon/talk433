import machine
from utime import ticks_ms, ticks_diff, sleep_ms, sleep


class Listener(object):

    def __init__(self, pin_number, pulse_len=10):
        print('initialize listner')
        self.rx_pin = machine.Pin(pin_number, machine.Pin.IN)
        self.signal = []
        self.byte = []
        b_led = machine.Pin(2, machine.Pin.OUT)
        r_led = machine.Pin(16, machine.Pin.OUT)
        timer = machine.Timer(0)
        timer.init(period=pulse_len, mode=machine.Timer.PERIODIC, callback=self._signal_received)

    def _process_signal(self, sign):
        if sign == [1,1,1,0]:
            self.byte.append(1)
        elif sign == [1,0,0,0]:
            self.byte.append(0)
        else:
            # print('signal was', sign)
            self.byte.append(None)
        if len(self.byte) == 8:
            print(self.byte)
            self.byte = []

    def _signal_received(self, value):
        v = self.rx_pin.value()
        # if value is 0 and last signal is empty skip
        if not len(self.signal) and v == 0:
            return False
        self.signal.append(v)
        if len(self.signal) == 4:
            self._process_signal(self.signal)
            self.signal = []
